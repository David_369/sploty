package me.david.sploty.exeptions;

public class UnsoportetColorExeption extends RuntimeException {

    public UnsoportetColorExeption(String color){
        super("Unsoportet Color: " + color);
    }
}
