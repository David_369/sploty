package me.david.sploty.exeptions;

public class ImagePareExeption extends RuntimeException {

    public ImagePareExeption(String message){
        super("Fehler beim Parsen eines Fotos: " + message);
    }
}
