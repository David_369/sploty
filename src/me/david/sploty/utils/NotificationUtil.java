package me.david.sploty.utils;

import me.david.sploty.Sploty;

import javax.swing.*;
import java.awt.*;

public class NotificationUtil {

    public static void sendNotifi(String title, String message, TrayIcon.MessageType type) {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        SystemTray tray = SystemTray.getSystemTray();
        Image image = Toolkit.getDefaultToolkit().createImage(Sploty.class.getResource("browser.png"));
        TrayIcon trayIcon = new TrayIcon(image, "Browser");
        trayIcon.setImageAutoSize(true);
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        trayIcon.displayMessage(title, message, type);
    }
}
