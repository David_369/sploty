package me.david.sploty.utils;

public class StringUtils {

    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }

    public static String replaceAllignorecase(String findtxt, String replacetxt, String str) {
        if (str == null) {
            return null;
        }
        if (findtxt == null || findtxt.length() == 0) {
            return str;
        }
        if (findtxt.length() > str.length()) {
            return str;
        }
        int counter = 0;
        while ((counter < str.length()) && (str.substring(counter).length() >= findtxt.length())) {
            String thesubstr = str.substring(counter, counter + findtxt.length());
            if (thesubstr.equalsIgnoreCase(findtxt)) {
                str = str.substring(0, counter) + replacetxt
                        + str.substring(counter + findtxt.length());
                return str;
            } else counter++;
        }
        return str;
    }
}
