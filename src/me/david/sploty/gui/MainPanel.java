package me.david.sploty.gui;

import me.david.sploty.Sploty;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {

    public Sploty sploty;
    public Tabs tabs;
    public Controll controll;

    public MainPanel(Sploty sploty){
        this.sploty = sploty;
        tabs = new Tabs(sploty);
        controll = new Controll(sploty);
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 0.3;
        gbc.fill = GridBagConstraints.BOTH;
        add(controll, gbc);
        gbc.gridy += 1;
        add(tabs, gbc);
    }
}
