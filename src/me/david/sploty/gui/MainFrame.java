package me.david.sploty.gui;

import me.david.sploty.Sploty;
import me.david.sploty.sites.Site;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;

public class MainFrame extends JFrame {

    public MainPanel mainPanel;
    public Sploty sploty;
    public static final Font MONOSPACED = new Font("Monospaced", Font.PLAIN, 12);

    public MainFrame(Sploty sploty){
        DisplayMode dm = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
        this.sploty = sploty;
        mainPanel = new MainPanel(sploty);
        setPreferredSize(new Dimension(dm.getWidth(), dm.getHeight()));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(mainPanel);
        String path = "/me/david/sploty/browser.png";
        setIconImage(new ImageIcon(getClass().getResource(path)).getImage());
        pack();
        setVisible(true);
        setAlwaysOnTop(true);
        setAlwaysOnTop(false);
        updateTitle();
        registershortcuts();
    }

    public void updateTitle(){
        final JTabbedPane tap = mainPanel.tabs;
        if(tap.getTabCount() == 0 || tap.getComponentAt(tap.getSelectedIndex()) == null || !(tap.getComponentAt(tap.getSelectedIndex()) instanceof Site))
            setTitle("Sploty");
        else setTitle(((Site) tap.getComponentAt(tap.getSelectedIndex())).getTitle() + " - Sploty");
    }

    public void registershortcuts(){
        getRootPane().getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_L,
                InputEvent.CTRL_MASK), "target_window");
        getRootPane().getActionMap().put("target_window", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainPanel.controll.input.requestFocus();
                mainPanel.controll.input.selectAll();
            }
        });
    }

}
