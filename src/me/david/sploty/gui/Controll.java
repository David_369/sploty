package me.david.sploty.gui;

import me.david.sploty.Sploty;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controll extends JPanel {

    public JTextField input;
    public JButton button;
    public Sploty sploty;

    public Controll(Sploty sploty){
        this.sploty = sploty;
        input = new JTextField("", 20);
        button = new JButton("Starten");
        button.setToolTipText("Seite öffnen");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                sploty.sitemanager.openUrl(input.getText());
            }
        });
        input.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                sploty.sitemanager.openUrl(input.getText());
            }
        });
        add(input);
        add(button);
    }
}
