package me.david.sploty.gui;

import me.david.sploty.Sploty;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.plaf.metal.MetalIconFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Tabs extends JTabbedPane {

    Sploty sploty;
    public JPopupMenu popupMenu;
    private Integer selectedTabIndex;

    public Tabs(Sploty sploty) {
        super(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        popupMenu = new JPopupMenu();
        popupMenu.add(new CloseAction("Close"));
        popupMenu.add(new CloseOthersAction("Close Others"));
        popupMenu.add(new CloseAllAction("Close All"));
        this.sploty = sploty;
        setUI(new BasicTabbedPaneUI() {
            private final Color SELECTED_TAB_COLOR = new Color(10, 36, 106);
            @Override
            protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex,
                                          int x, int y, int w, int h, boolean isSelected) {
                g.setColor(Color.GRAY);
                if (tabPlacement == BOTTOM)
                    g.drawLine(x, y + h, x + w, y + h);
                g.drawLine(x + w - 1, y, x + w - 1, y + h);
                if (tabPlacement == TOP) {
                    g.setColor(Color.WHITE);
                    g.drawLine(x, y, x, y + h);
                    g.drawLine(x, y, x + w - 2, y);
                }

                if (tabPlacement == BOTTOM && isSelected) {
                    g.setColor(Color.WHITE);
                    g.drawLine(x + 1, y + 1, x + 1, y + h);
                    g.drawLine(x + w - 2, y, x + w - 2, y + h);
                    g.drawLine(x + 1, y + 1, x + w - 2, y + 1);
                    g.drawLine(x + 1, y + h - 1, x + w - 2, y + h - 1);
                }
            }

            @Override
            protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex,
                                              int x, int y, int w, int h, boolean isSelected) {
                if (isSelected) {
                    if (tabPlacement == TOP) {
                        Graphics2D g2 = (Graphics2D) g;
                        Paint storedPaint = g2.getPaint();
                        g2.setPaint(new GradientPaint(x, y, SELECTED_TAB_COLOR, x + w, y + h, Color.ORANGE));
                        g2.fillRect(x, y, w, h);
                        g2.setPaint(storedPaint);
                    }
                } else {
                    g.setColor(Color.WHITE);
                    g.fillRect(x, y, w - 1, h);
                }
            }

            @Override protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) { return fontHeight + 12; }

            @Override
            protected void paintText(Graphics g, int tabPlacement, Font font,
                                     FontMetrics metrics, int tabIndex, String title, Rectangle textRect,
                                     boolean isSelected) {
                g.setColor(isSelected && tabPlacement == TOP?Color.WHITE:Color.BLACK);
                char[] chars = title.toCharArray();
                if(metrics.stringWidth(title) > textRect.width){
                    title = "";
                    for(char c : chars) {
                        title += c;
                        if(metrics.stringWidth(title) > textRect.width) break;
                    }
                }

                //Font tabFont = new Font("Tahoma", Font.PLAIN, 11);
                //g.setFont(tabFont);
                g.drawString(title, textRect.x, textRect.y + metrics.getAscent());
            }

            @Override
            protected void paintContentBorderTopEdge(Graphics g, int tabPlacement,
                                                     int selectedIndex, int x, int y, int w, int h) {
                if (selectedIndex != -1 && tabPlacement == TOP) {
                    g.setColor(Color.GRAY);
                    g.drawLine(x, y, x + w, y);
                }
            }

            @Override
            protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement,
                                                        int selectedIndex, int x, int y, int w, int h) {
                g.setColor(Color.GRAY);
                g.drawLine(x, y + h, x + w, y + h);
            }

            @Override
            protected int calculateTabWidth(int i, int i1, FontMetrics fontMetrics) {
                return (sploty.frame.getWidth()-200)/getTabCount();
            }
        });
        addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                sploty.frame.updateTitle();
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                if (mouseEvent.isPopupTrigger()) {
                    selectedTabIndex = indexAtLocation(mouseEvent.getX(), mouseEvent.getY());
                    if (getTabPlacement() == JTabbedPane.TOP)
                        popupMenu.show(Tabs.this, mouseEvent.getX(), mouseEvent.getY());
                }
            }

        });
    }

    private class CloseAction extends AbstractAction {

        private static final long serialVersionUID = -2625928077474199856L;

        public CloseAction(String name) {
            super(name);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            removeTabAt(selectedTabIndex);
        }
    }

    private class CloseOthersAction extends AbstractAction {

        private static final long serialVersionUID = -2625928077474199856L;

        public CloseOthersAction(String name) {
            super(name);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int tabCount = getTabCount();

            if (selectedTabIndex < tabCount - 1) {
                for (int i = selectedTabIndex + 1; i < tabCount; i++) {
                    removeTabAt(selectedTabIndex + 1);
                }
            }

            if (selectedTabIndex > 0) {
                for (int i = 0; i < selectedTabIndex; i++) {
                    removeTabAt(0);
                }
            }
        }
    }

    private class CloseAllAction extends AbstractAction {

        private static final long serialVersionUID = -2625928077474199856L;

        public CloseAllAction(String name) {
            super(name);
        }

        public void actionPerformed(ActionEvent actionEvent) {

            int tabCount = getTabCount();

            for (int i = 0; i < tabCount; i++) {
                removeTabAt(0);
            }
        }
    }

    public void addTabExit(String title, Icon icon, Component component, String tip) {
        super.addTab(title, icon, component, tip);
        int count = this.getTabCount() - 1;
        setTabComponentAt(count, new CloseButtonTab(component, title, icon));
    }

    public class CloseButtonTab extends JPanel {
        private Component tab;

        public CloseButtonTab(final Component tab, String title, Icon icon) {
            this.tab = tab;
            setOpaque(false);
            FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 3, 3);
            setLayout(flowLayout);
            JLabel jLabel = new JLabel(title);
            jLabel.setIcon(icon);
            add(jLabel);
            JButton button = new JButton(MetalIconFactory.getInternalFrameCloseIcon(16));
            button.setMargin(new Insets(0, 0, 0, 0));
            button.addMouseListener(new CloseListener(tab));
            add(button);
        }
    }
    public class CloseListener implements MouseListener
    {
        private Component tab;

        public CloseListener(Component tab){
            this.tab=tab;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if(e.getSource() instanceof JButton){
                JButton clickedButton = (JButton) e.getSource();
                Tabs tabbedPane = (Tabs) clickedButton.getParent().getParent().getParent();
                System.out.println(tabbedPane.selectedTabIndex);
                tabbedPane.remove(tab);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {}

        @Override
        public void mouseReleased(MouseEvent e) {}

        @Override
        public void mouseEntered(MouseEvent e) {
            if(e.getSource() instanceof JButton){
                JButton clickedButton = (JButton) e.getSource();
                //   clickedButton.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY,3));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if(e.getSource() instanceof JButton){
                JButton clickedButton = (JButton) e.getSource();
                //   clickedButton.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY,3));
            }
        }
    }

}
