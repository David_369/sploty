package me.david.sploty.sites;

public abstract class Parser implements Renderrer{

    public final String type;
    public final ParserType parsertype;

    public Parser(String type, ParserType parsertype){
        this.type = type;
        this.parsertype = parsertype;
    }

    /**
     * Getter for type
     *
     * @return value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Getter for parsertype
     *
     * @return value of parsertype
     */
    public ParserType getParsertype() {
        return parsertype;
    }
}
