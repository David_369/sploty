package me.david.sploty.sites;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SiteTools {

    public static ContentType getContentTypeHeader(URLConnection conn) {
        int i = 0;
        boolean moreHeaders;
        do {
            String headerName = conn.getHeaderFieldKey(i);
            String headerValue = conn.getHeaderField(i);
            if (headerName != null && headerName.equals("Content-Type"))
                return new ContentType(headerValue);

            i++;
            moreHeaders = headerName != null || headerValue != null;
        }
        while (moreHeaders);

        return null;
    }

    public static URL parseuser(String string){
        System.out.println("d1 " + string);
        if(string == null || string.isEmpty() || string.equals(" ")) return parse("https://google.com/");
        System.out.println("d2 " + string);
        if(!string.contains("."))return parse("https://google.com/?q=" + string);
        System.out.println("d3 " + string);
        try {
            return new URL(string);
        }catch (MalformedURLException e){
            e.printStackTrace();
        }
        System.out.println("d4 " + string);
        return null;
    }

    private static URL parse(String s){
        try {
            return new URL(s);
        }catch (MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    public static String getFilename(URL link){
        String url = link.getFile();
        if(url.contains("?"))url = url.split("\\?")[0];
        if(url.contains("/")){
            final String[] split = url.split("/");
            url = split[split.length-1];
        }
        return url;
    }

    public static boolean exits(URL url){
        try {
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("HEAD");
            return  huc.getResponseCode() == 200;
        }catch (IOException e){
            e.printStackTrace();
        }
        return false;
    }

}
