package me.david.sploty.sites;

import me.david.sploty.Sploty;
import me.david.sploty.sites.parser.DownloadParser;
import me.david.sploty.sites.parser.image.BmpImageParser;
import me.david.sploty.sites.parser.image.IcoImageParser;
import me.david.sploty.sites.parser.image.ImageParser;
import me.david.sploty.sites.parser.image.WindowsBmpImageParser;
import me.david.sploty.sites.parser.text.TextParser;
import me.david.sploty.sites.parser.text.html.HtmlParser;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

public class SiteManager {

    public final ArrayList<Parser> parsers = new ArrayList<Parser>();
    public final DownloadParser downloadparser;
    public Sploty sploty;

    public SiteManager(Sploty sploty) {
        downloadparser = new DownloadParser();
        parsers.add(new HtmlParser());
        parsers.add(new TextParser());
        parsers.add(new ImageParser());
        parsers.add(new IcoImageParser());
        parsers.add(new BmpImageParser());
        parsers.add(new WindowsBmpImageParser());
        this.sploty = sploty;
    }

    public void openUrl(URL url) {
        System.out.println(url.toString());
        HttpURLConnection.setFollowRedirects(true);
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            X509Certificate[] certs, String authType) {
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Site site = new Site(url);
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(15000);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Host", url.getHost());
            connection.setRequestProperty("User-Agent", "Spolty/2.0");
            connection.setRequestProperty("Accept", "image/gif, image/jpeg, */*");

            connection.connect();
            if (connection.getResponseCode() != 200) {
                System.out.println(connection.getResponseCode() + " | " + connection.getResponseMessage());
                site.setParser(new TextParser());
                JScrollPane error = new JScrollPane();
                JLabel code = new JLabel("Externer Feler Code " + connection.getResponseCode());
                JLabel message = new JLabel("Message " + connection.getResponseCode());
                code.setFont(new Font(code.getFont().getName(), Font.BOLD, 40));
                error.add(code);
                error.add(message);
                site.add(error);
                sploty.frame.mainPanel.tabs.addTab(site.getTitle(), site.getImage(), site);
                return;
            }
            site.setContentype(SiteTools.getContentTypeHeader(connection));
            site.setCharset(site.getContentype().getCharsetName() != null && site.getContentype().getCharsetName() != null && Charset.isSupported(site.getContentype().getCharsetName()) ?
                    Charset.forName(site.getContentype().getCharsetName()) : Charset.defaultCharset());

            if (site.getContentype().getContentType().contains("text/")) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), site.getCharset()));
                int n;
                StringBuilder content = new StringBuilder();
                while ((n = in.read()) != -1)
                    content.append((char) n);
                in.close();
                site.setContend(content.toString());
            }
            site.setParser(getParser(site.getContentype().getContentType()));
            site = site.getParser().render(site);

            URL iconurl = new URL(url.getProtocol() + "://" + url.getHost() + (url.getPort() == -1 ? "" : ":" + url.getPort()) + "/favicon.ico");
            if(SiteTools.exits(iconurl))site.setImage(IcoImageParser.getImageIcon(iconurl));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //for(Parser parser : parsers)
        //System.out.println(((ImageProducer)(url.getProtocol() + "://" + url.getHost() + (url.getPort() == -1?"" : ":" + url.getPort()) + "/favicon.ico")));
        sploty.frame.mainPanel.tabs.addTabExit(site.getTitle(), site.getImage(), site, site.getTitle());
        System.out.println(site.getContend());
    }

    public void openUrl(String url) {
        URL realurl = SiteTools.parseuser(url);
        if (realurl == null)
            try {
                realurl = new URL("https://google.com/?q=" + url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        openUrl(realurl);
    }


    public Parser getParser(String type) {
        type = type.toLowerCase();
        for (Parser parser : parsers)
            if (parser.getParsertype() == ParserType.EXACT && parser.getType().equals(type))
                return parser;
        for (Parser parser : parsers)
            if (parser.getParsertype() == ParserType.GENERELL && type.startsWith(parser.type))
                return parser;
        return downloadparser;
    }
}
