package me.david.sploty.sites.parser.text.html;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;
import me.david.sploty.utils.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.*;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlParser extends Parser{

    private final Pattern TITLE_TAG =
            Pattern.compile("\\<title>(.*)\\</title>", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);


    public HtmlParser(){
        super("text/html", ParserType.EXACT);
    }

    @Override
    public Site render(Site site) {
        Matcher matcher = TITLE_TAG.matcher(site.getContend());
        if (matcher.find())
            site.setTitle(matcher.group(1).replaceAll("[\\s\\<>]+", " ").trim());
        xmlReader(StringUtils.replaceAllignorecase("<!DOCTYPE html>", "", site.getContend()).replaceAll( "&([^;]+(?!(?:\\w|;)))", "&amp;$1" ));
        return site;
    }

    public void xmlReader(String contend){
        /*try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            InputSource inputSource = new InputSource(new StringReader(contend));

            xmlReader.setContentHandler(new XMLtoHTML());
            xmlReader.parse(inputSource);
            return xmlReader;
        }catch (IOException | SAXException e) {
            e.printStackTrace();
        }
        return null;*/
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(contend)));
            doc.getDocumentElement().normalize();
        }catch (IOException | SAXException | ParserConfigurationException e){
            e.printStackTrace();
        }
    }

    public class XMLtoHTML implements ContentHandler {

        private String currentValue;

        public void characters(char[] ch, int start, int length) throws SAXException {
            currentValue = new String(ch, start, length);
        }

        public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
            System.out.println(uri);
            System.out.println(localName);
            System.out.println(qName);
            System.out.println(currentValue);
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            System.out.println(uri);
            System.out.println(localName);
            System.out.println(qName);
            System.out.println(currentValue);
        }

        public void endDocument() throws SAXException {}
        public void endPrefixMapping(String prefix) throws SAXException {}
        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {}
        public void processingInstruction(String target, String data) throws SAXException {}
        public void setDocumentLocator(Locator locator) {  }
        public void skippedEntity(String name) throws SAXException {}
        public void startDocument() throws SAXException {}
        public void startPrefixMapping(String prefix, String uri) throws SAXException {}
    }
}
