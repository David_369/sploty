package me.david.sploty.sites.parser.text;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;
import me.david.sploty.utils.DebugUtil;

import javax.swing.*;

public class TextParser extends Parser {

    public TextParser(){
        super("text/", ParserType.GENERELL);
    }

    @Override
    public Site render(Site site) {
        JTextArea textArea = new JTextArea(site.getContend());
        site.setViewportView(textArea);
        //DebugUtil.send(site);
        return site;
    }

}
