package me.david.sploty.sites.parser.image;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;

import javax.swing.*;

public class WindowsBmpImageParser extends Parser {

    public WindowsBmpImageParser() {
        super("image/x-windows-bmp", ParserType.EXACT);
    }

    @Override
    public Site render(Site site) {
        site.setViewportView(new JLabel(BmpImageParser.getImageBmp(site.getUrl())));
        return site;
    }

}
