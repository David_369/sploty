package me.david.sploty.sites.parser.image;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;

import javax.swing.*;

public class ImageParser extends Parser {

    public ImageParser(){
        super("image/", ParserType.GENERELL);
    }

    @Override
    public Site render(Site site) {
        site.setViewportView(new JLabel(new ImageIcon(site.getUrl())));
        return site;
    }
}
