package me.david.sploty.sites.parser.image;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;
import net.sf.image4j.codec.ico.ICODecoder;
import net.sf.image4j.codec.ico.ICOImage;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class IcoImageParser extends Parser {

    public IcoImageParser() {
        super("image/x-icon", ParserType.EXACT);
    }

    @Override
    public Site render(Site site) {
        site.setViewportView(new JLabel(getImageIcon(site.getUrl())));
        return site;
    }

    public static ImageIcon getImageIcon(URL url) {
        try {
            InputStream is = url.openStream();
            List<BufferedImage> images = read(is);
            is.close();
            return new ImageIcon(images.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<BufferedImage> read(InputStream paramInputStream)
            throws IOException {
        List<ICOImage> localList = ICODecoder.readExt(paramInputStream);
        ArrayList<BufferedImage> localArrayList = new ArrayList<BufferedImage>(localList.size());
        for (ICOImage ico : localList) {
            BufferedImage localBufferedImage = ico.getImage();
            localArrayList.add(localBufferedImage);
        }
        return localArrayList;
    }

}
