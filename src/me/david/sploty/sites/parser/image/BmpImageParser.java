package me.david.sploty.sites.parser.image;

import me.david.sploty.exeptions.ImagePareExeption;
import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;
import net.sf.image4j.codec.bmp.BMPDecoder;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class BmpImageParser extends Parser {

    public BmpImageParser() {
        super("image/bmp", ParserType.EXACT);
    }

    @Override
    public Site render(Site site) {
        site.setViewportView(new JLabel(getImageBmp(site.getUrl())));
        return site;
    }

    public static ImageIcon getImageBmp(URL url){
        try {
            InputStream is = url.openStream();
            BufferedImage bg = BMPDecoder.read(is);
            is.close();
            return new ImageIcon(bg);
        }catch (IOException e){
            e.printStackTrace();
            throw new ImagePareExeption("bmp: " + e.getMessage());
        }
    }
}
