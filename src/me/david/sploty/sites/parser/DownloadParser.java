package me.david.sploty.sites.parser;

import me.david.sploty.sites.Parser;
import me.david.sploty.sites.ParserType;
import me.david.sploty.sites.Site;
import me.david.sploty.sites.SiteTools;
import me.david.sploty.utils.StringUtils;

import javax.swing.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class DownloadParser extends Parser {

    public DownloadParser() {
        super("", ParserType.NONE);
    }

    @Override
    public Site render(Site site) {
        String file = System.getProperty("user.home") + "/Downloads/" + SiteTools.getFilename(site.getUrl());
        int i = 0;
        System.out.println("a " + file);
        while (new File(file).exists()) {
            System.out.println(file);
            final String[] split = file.split("\\.");
            System.out.println(split.length);
            if (i == 0) file = split[split.length - 2] + "_(0)." + split[split.length - 1];
            else file = StringUtils.replaceLast(file, "_(" + (i - 1) + ")", "_(" + i + ")");
            i++;
        }
        download(site.getUrl().toString(), file);
        return site;
    }

    //@Override
    public JScrollPane render(Site site, JScrollPane panel) {
        String file = System.getProperty("user.home") + "/Downloads/" + SiteTools.getFilename(site.getUrl());
        int i = 0;
        System.out.println("a " + file);
        while (new File(file).exists()) {
            System.out.println(file);
            final String[] split = file.split("\\.");
            System.out.println(split.length);
            if (i == 0) file = split[split.length - 2] + "_(0)." + split[split.length - 1];
            else file = StringUtils.replaceLast(file, "_(" + (i - 1) + ")", "_(" + i + ")");
            i++;
        }
        System.out.println("a " + file);
        /*try {
            new File(file).createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(file)), site.getCharset()));
            out.write(site.getContend());
            out.close();
        }catch(IOException e){
            e.printStackTrace();
        }*/
        /*try (InputStream in = site.getUrl().openStream()) {
            Files.copy(in, new File(file).toPath(), StandardCopyOption.REPLACE_EXISTING);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        download(site.getUrl().toString(), file);
        return panel;
    }

    public static void download(String remotePath, String localPath) {
        BufferedInputStream in = null;
        FileOutputStream out = null;

        try {
            URL url = new URL(remotePath);
            URLConnection conn = url.openConnection();
            int size = conn.getContentLength();

            if (size < 0) System.out.println("Could not get the file size");
            else System.out.println("File size: " + size);

            in = new BufferedInputStream(url.openStream());
            out = new FileOutputStream(localPath);
            byte data[] = new byte[1024];
            int count;
            double sumCount = 0.0;
            int i = 0;
            while ((count = in.read(data, 0, 1024)) != -1) {
                out.write(data, 0, count);
                sumCount += count;
                if (size > 0) {
                    System.out.println("Percentace: " + (sumCount / size * 100) + "%");
                }
                i++;
            }
            System.out.println(i);
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            if (out != null)
                try {
                    out.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
        }
    }
}
