package me.david.sploty.sites;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentType {

    private static final Pattern CHARSET_HEADER = Pattern.compile("charset=([-_a-zA-Z0-9]+)", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);

    private String contentType;
    private String charsetName;

    public ContentType(String header) {
        if (header == null)
            throw new IllegalArgumentException("");
        int n = header.indexOf(";");
        if (n != -1) {
            contentType = header.substring(0, n);
            Matcher matcher = CHARSET_HEADER.matcher(header);
            if (matcher.find()) charsetName = matcher.group(1);
        } else contentType = header;
    }

    /**
     * Getter for CHARSET_HEADER
     *
     * @return value of CHARSET_HEADER
     */
    public static Pattern getCharsetHeader() {
        return CHARSET_HEADER;
    }

    /**
     * Getter for contentType
     *
     * @return value of contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Setter for {@see contentType}
     *
     * @param {@link contentType}
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Getter for charsetName
     *
     * @return value of charsetName
     */
    public String getCharsetName() {
        return charsetName;
    }

    /**
     * Setter for {@see charsetName}
     *
     * @param {@link charsetName}
     */
    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    public Charset getCharset() {
        return charsetName != null && Charset.isSupported(charsetName)?Charset.forName(charsetName):Charset.defaultCharset();
    }
}
