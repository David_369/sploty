package me.david.sploty.sites;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.nio.charset.Charset;

public class Site extends JScrollPane{

    private URL url;
    private ContentType contentype;
    private String title;
    private String contend;
    private Charset charset;
    private ImageIcon image;
    private Parser parser;

    public Site(URL url) {
        super();
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.url = url;
        title = url.getFile().contains("?") ? url.getFile().split("\\?")[0] : url.getFile();
    }

    /**
     * Getter for url
     *
     * @return value of url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * Setter for {@see url}
     *
     * @param {@link url}
     */
    public void setUrl(URL url) {
        this.url = url;
    }

    /**
     * Getter for contentype
     *
     * @return value of contentype
     */
    public ContentType getContentype() {
        return contentype;
    }

    /**
     * Setter for {@see contentype}
     *
     * @param {@link contentype}
     */
    public void setContentype(ContentType contentype) {
        this.contentype = contentype;
    }

    /**
     * Getter for title
     *
     * @return value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for {@see title}
     *
     * @param {@link title}
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for contend
     *
     * @return value of contend
     */
    public String getContend() {
        return contend;
    }

    /**
     * Setter for {@see contend}
     *
     * @param {@link contend}
     */
    public void setContend(String contend) {
        this.contend = contend;
    }

    /**
     * Getter for charset
     *
     * @return value of charset
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Setter for {@see charset}
     *
     * @param {@link charset}
     */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    /**
     * Getter for image
     *
     * @return value of image
     */
    public ImageIcon getImage() {
        return image;
    }

    /**
     * Setter for {@see image}
     *
     * @param {@link image}
     */
    public void setImage(ImageIcon image) {
        this.image = image;
    }

    /**
     * Getter for parser
     *
     * @return value of parser
     */
    public Parser getParser() {
        return parser;
    }

    /**
     * Setter for {@see parser}
     *
     * @param {@link parser}
     */
    public void setParser(Parser parser) {
        this.parser = parser;
    }

}
