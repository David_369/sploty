package me.david.sploty;

import me.david.sploty.gui.MainFrame;
import me.david.sploty.sites.SiteManager;

import java.util.logging.Level;

public class Sploty implements Runnable {

    public SiteManager sitemanager;
    public MainFrame frame;

    public static void main(String[] args){
        new Sploty().run();
    }

    @Override
    public void run() {
        sitemanager = new SiteManager(this);
        frame = new MainFrame(this);
        sitemanager.openUrl("https://google.com/");
    }

    public void log(Level level, String message){
        System.out.println("[" + level.getName() + "] " + message);
    }

}
